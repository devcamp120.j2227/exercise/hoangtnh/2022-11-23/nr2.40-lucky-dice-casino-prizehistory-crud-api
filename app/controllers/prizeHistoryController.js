//import thư viên mongoose
const mongoose = require("mongoose");
//import prize history model 
const prizeHisModel = require("../models/prizeHistoryModel");

//function create prize history
const createPrizeHistory = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    const body = request.body;
    //B2: validate dữ liệu
    //B3: gọi model tạo dữ liệu
    const newPrizeHistory = {
        _id: mongoose.Types.ObjectId(),
        user: mongoose.Types.ObjectId(),
        prize: mongoose.Types.ObjectId(),
        createAt: body.createAt,
        updateAt: body.updatedAt
    }
   
    prizeHisModel.create(newPrizeHistory, (error, data)=>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:"Create new prize history successfully",
            data: data
        })
    })
}
//function get all prize histories
const getAllPrizeHistory = (request, response) => {
    prizeHisModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all prize histories successfully",
            data: data
        })
    })
}

//function get prize history by id
const getPrizeHistoryById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const prizeHisId = request.params.prizeHisId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeHisId)){
        return response.status(400).json({
            status:"Bad Request",
            message: "prize history id không hợp lệ"
        })
    }
    //B3: gọi model chưa id prize 
    prizeHisModel.findById(prizeHisId,(error,data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`Get prize history by id ${prizeHisId} successfully`,
            data: data
        })
    })
}

//function update prize history by id
const updatePrizeHistoryById = (request, response) => {
    //B1: chuẩn bị dữ liệu'
    const prizeHisId = request.params.prizeHisId;
    const body = request.body;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeHisId)){
        return response.status(400).json({
            status: "Bad request",
            message: "Prize History Id không đúng"
        })
    }
    //B3: gọi model chưa id để tìm và update dữ liệu
    const updatePrizeHistory = {};
    if(body.user !== undefined){
        updatePrizeHistory.user = mongoose.Types.ObjectId();
    }
    if(body.prize !== undefined){
        updatePrizeHistory.prize = mongoose.Types.ObjectId();
    }
    prizeHisModel.findByIdAndUpdate(prizeHisId, updatePrizeHistory, (error, data)=>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:"Update Prize History successfully",
            data: data
        })
    })
}

//function delete prize history by id
const deletePrizeHistoryById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const prizeHisId = request.params.prizeHisId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeHisId)){
        return response.status(400).json({
            status:" Bad request",
            message:"Prize history id không đúng"
        })
    }
    //B3: gọi prize model chưa id cần xóa
    prizeHisModel.findByIdAndRemove(prizeHisId,(error, data) => {
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`delete Prize History had id ${prizeHisId} successfully`
        })
    })
}
module.exports = {
    createPrizeHistory,
    getAllPrizeHistory,
    getPrizeHistoryById,
    updatePrizeHistoryById,
    deletePrizeHistoryById
}